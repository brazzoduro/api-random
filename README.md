# API - Data Random

Api-Data Random es un API desarrollada en NodeJs y MongoDb bajo el marco flexible que nos brinda [Express](http://expressjs.com/) con métodos de utilidad HTTP y middleware a su disposición. Cuenta con la libreria [dreamjs](https://github.com/adleroliveira/dreamjs) para generar data random que permite al usuario poder realizar consultas al API con data autogenerada.

### Requisitos

API-DataRandom para correr requiere la instalacion previa de las versiones minimas de:
    a) [Node.js](https://nodejs.org/) v9.2.0.
    b) [MongoDb](https://www.mongodb.com/) v3.6.4. (Iniciar)
### Instalación y uso.
1) Ingresar al directorio 'api/' y luego instalar dependencias de la siguiente manera:
```sh
$ cd api
$ npm install       // Instalar librerias
```
2) Configuración global:
```sh
{
  "server_port": 3000,          // Puerto de escucha para el servidor
  "server_name": "localhost",   // Nombre del servidor
  "db_name": "dbPrueba",        // Nombre de la pase de datos
  "db_port": "27017"            // Puerto del servidor MongoDb
}
```
La configuración global se puede encontrar en el archivo app/config.json.

3) Configurar semilla.
```sh
$ npm run seed      // Crear Db y generar data random
```
Este comando creara la conexion con mongodb, consumira la libreria dreamjs para generar la data random y luego va a cargarla en una base de datos previamente configurada (2.Configuración global). Nota: Mongodb debe encontrarse previamente iniciado para que el compilador no arroje errores.

4) Iniciar API:
```sh
$ npm start         // Inicia el servidor
```

Luego de iniciar, podrá observar el siguiente mensaje:
```sh
$ npm start         // Inicia el servidor
Name: API - Data Random
Database Name: dbPrueba
Server: http://localhost:3000
Server started and waiting for application at port 3000!

 To STOP press ctrl + c
-----------------------------------------------------------
```
Esto indica que todo va bien. Ahora sólo queda realizar consultas.

##### USO
Las consultas se pueden realizar de la siguiente manera:
    `GET /count?edad=20&grupo=region`

Endpoint: '/count' es la ruta (Endpoint).
#### Parametros:
    'edad'  es la edad a consultar (Parametro 1)
    'grupo' es el tipo de consulta 'region' ó 'genero' (Parametro 2)
Resultado ejempo al realizar una la consulta con el parametro (genero):
```sh
{
    hombre: {suma: 2},
    mujer: {suma: 1}
}
```
En el servidor:
```
GET /count?edad=20&grupo=genero 304 210.524 ms - -
```

Resultado ejempo al realizar una la consulta con el parametro (region):
```sh
{
...
    9: {suma: 0},
    11: {suma: 0},
    12: {suma: 1},
    13: {suma: 0},
...
}
```
En el servidor:
```
GET /count?edad=20&grupo=region 304 37.322 ms - -
```
License
----

MIT - brazzoduro26@gmail.com
