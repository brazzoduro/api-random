const assert = require('assert');
const data = require('./data_random');

const connection = require('../core/connection');

connection(function (db, end) {
  db.collection('users').drop();
  db.collection('users').insertMany(data, function (err, r) {
    assert.equal(null, err);
    assert.equal(data.length, r.insertedCount);

    end();
  });
});

