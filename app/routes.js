const express = require('express');
const router = express.Router();
const connection = require('./core/connection');

router.get('/count', function (request, response) {
  connection(function (db, end) {

    const data = db.collection('users').aggregate([
      {
        $group: {
          _id: '$' + request.query.grupo,
          suma: {
            $sum: {
              $cond: {
                if: {
                  $eq: ['$edad', parseInt(request.query.edad, 10)]
                },
                then: 1,
                else: 0
              }
            }
          }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ]).toArray();

    data.then(function (users) {
      response.json(users.reduce(function (now, value) {
        now[value._id] = {
          suma: value.suma
        };
        return now;
      }, {}));
      end();
    });
  });
});

module.exports = router;
