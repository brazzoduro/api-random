const mongo = require('mongodb').MongoClient;
const assert = require('assert');
const global = require('../config');
const url = 'mongodb://' + global.server_name + ':' + global.db_port;

module.exports = function (callback) {

  mongo.connect(url, { useNewUrlParser: true }, function (err, client) {
    assert.equal(null, err);
    const db = client.db(global.db_name);

    callback(db, function () {
      client.close();
    });
  });
};
